import "../scss/app.scss";

  // This block will be executed once the page is loaded and ready
window.addEventListener("DOMContentLoaded", () => {
    // Gets the price
  const price = document.getElementsByClassName('subtitle is-6 price')[0].textContent;
    //Sets the price
  const product = document.getElementsByClassName('card product')[0];
  product.setAttribute('data-price',price);
});

